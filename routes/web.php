<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('change-language/{languageCode}', 'Web\LanguageController@change');

Route::middleware(['web','auth'])->group(function () {
    Route::get('/', function () {
        return redirect()->to('/dashboard');
    })->name('index');

    Route::get('/back', function () {
        return back()->with('hide_back',true);
    })->name('portlet-back');

    Route::get('/dashboard', 'Web\Dashboard\MainController@index')->name('dashboard');;

    Route::get('/vehicles', 'Web\Vehicles\MainController@index')->name('vehicles-main');
    Route::get('/vehicles/new-vehicle', 'Web\Vehicles\MainController@indexNewVehicle')->name('new-vehicle-index');
    Route::get('/vehicles/vehicle-assignments', 'Web\Vehicles\MainController@indexVehicleAssignment')->name('vehicle-assignments-index');
    Route::get('/vehicles/new-vehicle-assignment', 'Web\Vehicles\MainController@indexNewVehicleAssignment')->name('vehicle-assignments-index');
    Route::get('/vehicles/vehicle-un-assignment', 'Web\Vehicles\MainController@indexVehicleUnAssignment')->name('vehicle-un-assignments-index');

    Route::get('/issues', 'Web\Issues\MainController@index')->name('issues-main');
    Route::get('/issues/new-issue', 'Web\Issues\MainController@indexNewIssue')->name('new-issue-index');

    Route::get('/reminders', 'Web\Reminders\MainController@index')->name('reminders-main');
    Route::get('/reminders/vehicle-reminders', 'Web\Reminders\MainController@indexVehicleReminders')->name('vehicle-reminders-index');
    Route::get('/reminders/new-vehicle-reminder', 'Web\Reminders\MainController@indexNewVehicleReminder')->name('new-vehicle-reminder-index');
    Route::get('/reminders/service-reminders', 'Web\Reminders\MainController@indexServiceReminders')->name('service-reminders-index');
    Route::get('/reminders/new-service-reminder', 'Web\Reminders\MainController@indexNewServiceReminder')->name('new-service-reminder-index');
    Route::get('/reminders/contact-reminders', 'Web\Reminders\MainController@indexContactReminders')->name('contact-reminders-index');
    Route::get('/reminders/new-contact-reminder', 'Web\Reminders\MainController@indexNewContactReminder')->name('new-contact-reminder-index');

    Route::get('/fuel', 'Web\Fuel\MainController@index')->name('fuel-main');
    Route::get('/fuel/new-fuel-entry', 'Web\Fuel\MainController@indexNewFuelEntry')->name('new-fuel-entry-index');

    Route::get('/contacts', 'Web\Contacts\MainController@index')->name('contacts-main');
    Route::get('/contacts/companies', 'Web\Contacts\MainController@indexCompanies')->name('contacts-company-index');
    Route::get('/contacts/new-company', 'Web\Contacts\MainController@indexNewCompany')->name('new-company-index');
    Route::get('/contacts/people', 'Web\Contacts\MainController@indexPeople')->name('contacts-people-index');
    Route::get('/contacts/new-people', 'Web\Contacts\MainController@indexNewPeople')->name('new-people-index');

    Route::get('/reports', 'Web\Reports\MainController@index')->name('reports-main');

    Route::get('/accounts', 'Web\Accounts\MainController@index')->name('accounts-main');

    Route::get('/settings', 'Web\Settings\MainController@index')->name('settings-main');
    Route::get('/settings/edit-profile', 'Web\Settings\MainController@indexProfileEdit')->name('edit-profile');
    Route::get('/settings/users', 'Web\Settings\MainController@indexUsers')->name('manage-users');
    Route::get('/settings/groups-permissions', 'Web\Settings\MainController@indexGroupPermissions')->name('groups-permissions');


    Route::get('/help', 'Web\Help\MainController@index')->name('help-main');
});

Auth::routes(['register' => false]);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
