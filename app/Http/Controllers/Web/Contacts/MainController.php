<?php

namespace App\Http\Controllers\Web\Contacts;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {
        return view('pages.contacts.sub-pages.landing');
    }

    public function indexCompanies()
    {
        return view('pages.contacts.sub-pages.company-landing');
    }

    public function indexPeople()
    {
        return view('pages.contacts.sub-pages.people-landing');
    }

    public function indexNewCompany()
    {
        return view('pages.contacts.sub-pages.new-company');
    }

    public function indexNewPeople()
    {
        return view('pages.contacts.sub-pages.new-person');
    }
}
