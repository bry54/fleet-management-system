<?php

namespace App\Http\Controllers\Web\Reminders;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {
        return view('pages.reminders.sub-pages.landing');
    }

    public function indexServiceReminders()
    {
        return view('pages.reminders.sub-pages.service-reminders');
    }

    public function indexNewServiceReminder()
    {
        return view('pages.reminders.sub-pages.new-service-reminder');
    }

    public function indexVehicleReminders()
    {
        return view('pages.reminders.sub-pages.vehicle-reminders');
    }

    public function indexNewVehicleReminder()
    {
        return view('pages.reminders.sub-pages.new-vehicle-reminder');
    }

    public function indexContactReminders()
    {
        return view('pages.reminders.sub-pages.contact-reminders');
    }

    public function indexNewContactReminder()
    {
        return view('pages.reminders.sub-pages.new-contact-reminder');
    }
}
