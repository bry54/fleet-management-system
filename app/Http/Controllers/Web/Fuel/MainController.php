<?php

namespace App\Http\Controllers\Web\Fuel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {
        return view('pages.fuel.sub-pages.landing');
    }

    public function indexNewFuelEntry()
    {
        return view('pages.fuel.sub-pages.new-fuel-entry');
    }
}
