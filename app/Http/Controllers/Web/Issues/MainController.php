<?php

namespace App\Http\Controllers\Web\Issues;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {
        return view('pages.issues.sub-pages.landing');
    }

    public function indexNewIssue()
    {
        return view('pages.issues.sub-pages.new-issue');
    }
}
