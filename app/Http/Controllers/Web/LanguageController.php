<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class LanguageController extends Controller
{
    public function change($languageCode)
    {
        session(['app_locale' => $languageCode]);
        App::setLocale(Session::get('app_locale'));
        return redirect()->back();
    }
}
