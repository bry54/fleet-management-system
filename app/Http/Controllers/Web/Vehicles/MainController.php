<?php

namespace App\Http\Controllers\Web\Vehicles;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {
        return view('pages.vehicles.sub-pages.landing');
    }

    public function indexNewVehicle()
    {
        return view('pages.vehicles.sub-pages.new-vehicle');
    }

    public function indexVehicleAssignment()
    {
        return view('pages.vehicles.sub-pages.vehicle-assignments');
    }

    public function indexNewVehicleAssignment()
    {
        return view('pages.vehicles.sub-pages.new-vehicle-assignment');
    }

    public function indexVehicleUnAssignment()
    {
        return view('pages.vehicles.sub-pages.vehicle-un-assignment');
    }
}
