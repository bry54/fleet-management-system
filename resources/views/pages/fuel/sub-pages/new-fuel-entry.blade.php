@extends('pages.fuel.main')

@section('additional-items')

@endsection

@section('sub-page')
    <div id="new-issue">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="flaticon2-writing"></i>
                </span>
                    <h3 class="kt-portlet__head-title">
                        {{ __('fuel.lbl_new_fuel_entry') }}
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">

                </div>
            </div>
            <div class="kt-portlet__body row">
                <div class="col-8 offset-2">
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label>Vehicle:</label>
                            <input type="text" class="form-control" placeholder="Select Vehicle">
                        </div>
                        <div class="col-lg-6">
                            <label class="">Date and Time:</label>
                            <input type="datetime-local" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label>Odometer Reading:</label>
                            <div class="kt-input-icon">
                                <input type="text" class="form-control" placeholder="Enter odometer reading before fillup">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <label>Fill up :</label>
                            <div class="kt-radio-inline">
                                <label class="kt-radio kt-radio--solid">
                                    <input type="radio" name="example_2" checked="" value="2"> Partial Fill Up
                                    <span></span>
                                </label>
                                <label class="kt-radio kt-radio--solid">
                                    <input type="radio" name="example_2" value="2"> Full Tank Fill Up
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label class="">Vendor:</label>
                            <input type="text" class="form-control" placeholder="Enter fuel vendor">
                        </div>
                        <div class="col-lg-6">
                            <label>Invoice Number:</label>
                            <input type="text" class="form-control" placeholder="Enter invoice number">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-4">
                            <label>Price/Unit:</label>
                            <input type="text" class="form-control" placeholder="Enter Price per unit">
                        </div>
                        <div class="col-lg-4">
                            <label class="">Volume Filled Up:</label>
                            <input type="text" class="form-control" placeholder="Enter volume filled up">
                        </div>
                        <div class="col-lg-4">
                            <label class="">Fuel Type/Grade:</label>
                            <input type="text" class="form-control" placeholder="Select Fuel type">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label for="exampleTextarea">Comment</label>
                            <textarea class="form-control" rows="3"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__foot">
                <div class="row align-items-right">
                    <div class="col-lg-12 kt-align-right">
                        <button type="submit" class="btn btn-brand btn-sm">{{ __('global.common_labels.save')}}</button>
                        <button type="button" class="btn btn-default btn-sm">{{ __('global.common_labels.cancel')}}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        let app = new Vue({
            el: '#new-issue',
            data: {
                vehicles: null
            },

            created: function () {

            },

            mounted: function () {

            },

            computed: {

            },

            watch: {

            },

            methods: {

            }


        })
    </script>
@endsection
