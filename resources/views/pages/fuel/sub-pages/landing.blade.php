@extends('pages.fuel.main')

@section('sub-page')
    <div id="fuel-landing">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-google-drive-file"></i>
										</span>
                    <h3 class="kt-portlet__head-title">
                        {{ __('fuel.menu_title') }}
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <div class="kt-portlet__head-actions">
                            <a href="{{ url('fuel/new-fuel-entry') }}" class="btn btn-default btn-icon-sm">
                                <i class="la la-plus"></i> {{ __('fuel.btn_new_entry') }}
                            </a>
                            <div class="dropdown dropdown-inline" v-if="fuelEntries && fuelEntries.length">
                                <button type="button" class="btn btn-default btn-icon-sm dropdown-toggle"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="la la-download"></i> {{ __('global.common_labels.export') }}
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <ul class="kt-nav">
                                        <li class="kt-nav__section kt-nav__section--first">
                                            <span class="kt-nav__section-text">{{ __('global.common_labels.choose') }}</span>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon la la-print"></i>
                                                <span class="kt-nav__link-text">{{ __('global.common_labels.print') }}</span>
                                            </a>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon la la-file-excel-o"></i>
                                                <span class="kt-nav__link-text">{{ __('global.common_labels.excel') }}</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">
                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1"
                       v-if="fuelEntries && fuelEntries.length">
                    <thead>
                    <tr>
                        <th>{{ __('fuel.tbl_vehicle') }}</th>
                        <th>{{ __('fuel.tbl_date_time') }}</th>
                        <th>{{ __('fuel.tbl_usage') }}</th>
                        <th>{{ __('fuel.tbl_volume') }}</th>
                        <th>{{ __('fuel.tbl_total') }}</th>
                        <th>{{ __('fuel.tbl_fuel_economy') }}</th>
                        <th>{{ __('fuel.tbl_fuel_cost') }}</th>
                        <th>{{ __('global.common_labels.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="fuelEntry in fuelEntries">
                        <td>
                            <h6 class="form-text kt-font-transform-u">@{{ fuelEntry.vehicle.name }}</h6>
                            <span class="form-text text-black-50">Licence Plate: <span class="kt-font-transform-u text-muted">@{{ fuelEntry.vehicle.plate }}</span></span>
                        </td>
                        <td>
                            <span class="form-text">@{{ fuelEntry.date }}</span>
                            <span class="form-text"><i class="fa fa-map-marker text-warning" style="margin-right: 5px"></i>@{{ fuelEntry.vendor }}</span>
                        </td>
                        <td>
                            <span class="form-text">@{{ fuelEntry.usage }}</span>
                            <span class="form-text"><abbr title="Current mileage">@{{ fuelEntry.current_mileage }}</abbr></span>
                        </td>
                        <td>
                            <span class="form-text">@{{ fuelEntry.volume }}</span>
                            <span class="form-text"><i class="fa fa-info text-info" style="margin-right: 5px"></i>@{{ fuelEntry.fuel_type }}</span>
                        </td>
                        <td>
                            <span class="form-text">@{{ fuelEntry.total }}</span>
                            <span class="form-text"><i class="fa fa-dollar-sign text-success" style="margin-right: 5px"></i>@{{ fuelEntry.price_per_unit }}</span>
                        </td>
                        <td>
                            <span class="form-text">@{{ fuelEntry.fuel_economy }}</span>
                        </td>
                        <td>
                            <span class="form-text">@{{ fuelEntry.fuel_cost }}</span>
                        </td>
                        <td data-field="Actions" data-autohide-disabled="false" class="kt-datatable__cell">
                        <span style="overflow: visible; position: relative; width: 110px;">
                            <div class="dropdown">
                                <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                                    <i class="la la-edit"></i>
                                </a>

                                <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                                    <i class="la la-trash"></i>
                                </a>
                            </div>
                        </span>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <!--end: Datatable -->

                <div v-else class="text-center">
                    <h3 class="display-4">
                        {{ __('fuel.msg_no_entries') }}
                    </h3>

                    <a href="{{ url('fuel/new-fuel-entry') }}" class="btn btn-primary btn-upper kt-margin-50">
                        {{ __('fuel.btn_create_entry') }}
                    </a>
                </div>
            </div>
        </div>
    </div>

    <script>
        let app = new Vue({
            el: '#fuel-landing',
            data: {
                fuelEntries: [{
                    vehicle:{
                        name: 'Land Rover Defender',
                        plate: 'AS-1234-HRE',
                    },
                    date: '09.01.2020 09:00',
                    vendor: 'Shell',
                    usage: '200 KM',
                    volume: '8 Ltrs',
                    fuel_type: 'Diesel',
                    price_per_unit: '3.55/ltr',
                    total: '$28.40',
                    fuel_economy: '20KM/lt',
                    fuel_cost: '$1.00/km',
                    current_mileage: '100 KM',
                }]
            },

            created: function () {

            },

            mounted: function () {

            },

            computed: {

            },

            watch: {

            },

            methods: {

            }


        })
    </script>
@endsection
