@extends('layouts.master')

@section('page-title', __('fuel.menu_title') )

@section('page-header-title', __('fuel.page_header_title') )

@section('header-left')
    @include('pages.fuel.header-left')
@endsection

@section('header-right')
    @include('pages.fuel.header-right')
@endsection

@section('content')
    @yield('sub-page')
@endsection