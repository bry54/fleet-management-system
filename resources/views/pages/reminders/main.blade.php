@extends('layouts.master')

@section('page-title', __('reminders.menu_title') )

@section('page-header-title', __('reminders.page_header_title') )

@section('header-left')
    @include('pages.reminders.header-left')
@endsection

@section('header-right')
    @include('pages.reminders.header-right')
@endsection

@section('content')
    @yield('sub-page')
@endsection