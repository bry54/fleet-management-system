@extends('pages.reminders.main')

@section('sub-page')
    <style>
        .help-text {
            padding: 0rem 1.75rem
        }
    </style>

    <div id="reminders-landing">
        <div class="row">
            <div class="col-lg-4">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon">
						<i class="flaticon-calendar"></i>
					</span>
                            <h3 class="kt-portlet__head-title kt-font-primary">
                                Top Service Reminders
                            </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <div class="kt-portlet__head-actions">
                                <a href="#" class="btn btn-label  btn-sm">
                                    See All
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        Some data here
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon">
						<i class="flaticon-calendar"></i>
					</span>
                            <h3 class="kt-portlet__head-title kt-font-primary">
                                Top Vehicle Reminders
                            </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <div class="kt-portlet__head-actions">
                                <a href="#" class="btn btn-label  btn-sm">
                                    See All
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        Some data here
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon">
						<i class="flaticon-calendar"></i>
					</span>
                            <h3 class="kt-portlet__head-title kt-font-primary">
                                Top Contacts Reminders
                            </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <div class="kt-portlet__head-actions">
                                <a href="#" class="btn btn-label  btn-sm">
                                    See All
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        Some data here
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        let app = new Vue({
            el: '#reminders-landing',
            data: {
                vehicles: null
            },

            created: function () {

            },

            mounted: function () {

            },

            computed: {

            },

            watch: {

            },

            methods: {

            }


        })
    </script>
@endsection
