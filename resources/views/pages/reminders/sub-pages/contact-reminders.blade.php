@extends('pages.reminders.main')

@section('sub-page')
    <div id="contact-reminders">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-google-drive-file"></i>
										</span>
                    <h3 class="kt-portlet__head-title">
                        {{ __('reminders.contacts.portlet_title') }}
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <div class="kt-portlet__head-actions">
                            <a href="{{ url('reminders/new-contact-reminder') }}" class="btn btn-default btn-icon-sm">
                                <i class="la la-plus"></i> {{ __('reminders.contacts.new_reminder') }}
                            </a>
                            <div class="dropdown dropdown-inline" v-if="reminders && reminders.length">
                                <button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="la la-download"></i> {{ __('global.common_labels.export') }}
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <ul class="kt-nav">
                                        <li class="kt-nav__section kt-nav__section--first">
                                            <span class="kt-nav__section-text">{{ __('global.common_labels.choose') }}</span>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon la la-print"></i>
                                                <span class="kt-nav__link-text">{{ __('global.common_labels.print') }}</span>
                                            </a>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon la la-file-excel-o"></i>
                                                <span class="kt-nav__link-text">{{ __('global.common_labels.excel') }}</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">
                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1" v-if="reminders && reminders.length">
                    <thead>
                    <tr>
                        <th>{{ __('contacts.tbl_matter') }}</th>
                        <th>{{ __('contacts.tbl_client') }}</th>
                        <th>{{ __('contacts.tbl_responsible_solicitor') }}</th>
                        <th>{{ __('contacts.tbl_originating_solicitor') }}</th>
                        <th>{{ __('contacts.tbl_practise_area') }}</th>
                        <th>{{ __('contacts.tbl_open_date') }}</th>
                        <th>{{ __('global.common_labels.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>

                    </tr>
                    </tbody>
                </table>
                <!--end: Datatable -->

                <div v-else class="text-center">
                    <h3 class="display-4">{{ __('reminders.contacts.msg_no_reminders') }}</h3>

                    <a href="{{ url('reminders/new-contact-reminder') }}" class="btn btn-primary btn-upper kt-margin-50">
                        {{ __('reminders.contacts.new_reminder') }}
                    </a>
                </div>
            </div>
        </div>
    </div>

    <script>
        let app = new Vue({
            el: '#contact-reminders',
            data: {
                reminders: null
            },

            created: function () {

            },

            mounted: function () {

            },

            computed: {

            },

            watch: {

            },

            methods: {

            }


        })
    </script>
@endsection
