<div class="kt-subheader__main">
    @if(request()->route()->getName() !== 'reminders-main')
        <a href="{{ route('reminders-main') }}" class="btn btn-outline-dark btn-bold btn-sm btn-icon-h">
            <i class="fa fa-home"></i>
            <span class="kt-hidden-mobile">{{__('reminders.btn_home')}}</span>
        </a>
        <div style="margin: 0 10px;"></div>
        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
    @endif

    {{--<h3 class="kt-subheader__title">{{ __('reminders.menu_title') }}</h3>--}}

        <a href="{{url('reminders/vehicle-reminders')}}"
           class="btn btn-outline-info {{ request()->is('reminders/vehicle-reminders') ? 'active' : '' }}">
            {{__('reminders.btn_vehicles')}}
        </a>

        <a href="{{url('reminders/service-reminders')}}"
           class="btn btn-outline-info {{ request()->is('reminders/service-reminders') ? 'active' : '' }}">
            {{__('reminders.btn_service')}}
        </a>

        <a href="{{url('reminders/contact-reminders')}}"
           class="btn btn-outline-info {{ request()->is('reminders/contact-reminders') ? 'active' : '' }}">
            {{__('reminders.btn_contacts')}}
        </a>
</div>
