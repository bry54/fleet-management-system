@extends('layouts.master')

@section('page-title', __('help.menu_title') )

@section('page-header-title', __('help.page_header_title') )

@section('header-left')
    @include('pages.help.header-left')
@endsection

@section('header-right')
    @include('pages.help.header-right')
@endsection

@section('content')

@endsection