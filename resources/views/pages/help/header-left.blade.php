<div class="kt-subheader__main">
    @if(request()->route()->getName() !== 'help-main')
        <a href="{{ route('help-main') }}" class="btn btn-outline-dark btn-bold btn-sm btn-icon-h">
            <i class="fa fa-home"></i>
            <span class="kt-hidden-mobile">{{__('help.btn_home')}}</span>
        </a>
        <div style="margin: 0 10px;"></div>
        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
    @endif

    {{--<h3 class="kt-subheader__title">{{ __('help.menu_title') }}</h3>--}}
</div>