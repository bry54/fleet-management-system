@extends('layouts.master')

@section('page-title', __('dashboard.menu_title') )

@section('page-header-title', __('dashboard.page_header_title') )

@section('header-left')
    @include('pages.dashboard.header-left')
@endsection

@section('header-right')
    @include('pages.dashboard.header-right')
@endsection

@section('content')
    <div>
        <div class="kt-section__content kt-section__content--solid">
            <h3 class="kt-font-transform-u">
                {{ __('dashboard.agenda.main_header') }}
            </h3>
        </div>
        <div class="row">
            Dashboard content
        </div>
    </div>

@endsection
