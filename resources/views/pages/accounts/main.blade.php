@extends('layouts.master')

@section('page-title', __('accounts.menu_title'))

@section('header-left')
    @include('pages.accounts.header-left')
@endsection

@section('header-right')
    @include('pages.accounts.header-right')
@endsection

@section('content')
    @yield('sub-page')
@endsection