<div class="kt-subheader__toolbar">
    <div class="kt-subheader__wrapper">
        <a href="#" class="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10">
            {{ __('global.common_labels.export') }}
        </a>
    </div>
</div>
