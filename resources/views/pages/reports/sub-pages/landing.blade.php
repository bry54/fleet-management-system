@extends('pages.reports.main')

@section('sub-page')
    <style>
        .kt-notification__item-title  {
            font-size: 1.25rem !important;
        }
        .kt-notification__item-time {
            font-size: 1.0rem !important;
        }
    </style>
    <div class="row">
        <div class="col-lg-6">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            {{ __('reports.vehicle_reports.heading') }}
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="kt-notification kt-notification--fit">
                        <a href="#" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                -
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-transform-c">
                                    {{ __('reports.vehicle_reports.vehicle_list_title') }}
                                </div>
                                <div class="kt-notification__item-time">
                                    {{ __('reports.vehicle_reports.vehicle_list_subtitle') }}
                                </div>
                            </div>
                        </a>
                        <a href="#" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                -
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-transform-c">
                                    {{ __('reports.vehicle_reports.vehicle_details_title') }}
                                </div>
                                <div class="kt-notification__item-time">
                                    {{ __('reports.vehicle_reports.vehicle_details_subtitle') }}
                                </div>
                            </div>
                        </a>
                        <a href="#" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                -
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-transform-c">
                                    {{ __('reports.vehicle_reports.utilization_summary_title') }}
                                </div>
                                <div class="kt-notification__item-time">
                                    {{ __('reports.vehicle_reports.utilization_summary_subtitle') }}
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            {{ __('reports.contact_reports.heading') }}
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="kt-notification kt-notification--fit">
                        <a href="#" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                -
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-transform-c">
                                    {{ __('reports.contact_reports.contacts_list_title') }}
                                </div>
                                <div class="kt-notification__item-time">
                                    {{ __('reports.contact_reports.contacts_list_subtitle') }}
                                </div>
                            </div>
                        </a>
                        <a href="#" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                -
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-transform-c">
                                    {{ __('reports.contact_reports.contact_renewal_title') }}
                                </div>
                                <div class="kt-notification__item-time">
                                    {{ __('reports.contact_reports.contact_renewal_subtitle') }}
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            {{ __('reports.fuel_reports.heading') }}
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="kt-notification kt-notification--fit">
                        <a href="#" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                -
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-transform-c">
                                    {{ __('reports.fuel_reports.fuel_summary_title') }}
                                </div>
                                <div class="kt-notification__item-time">
                                    {{ __('reports.fuel_reports.fuel_summary_subtitle') }}
                                </div>
                            </div>
                        </a>
                        <a href="#" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                -
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-transform-c">
                                    {{ __('reports.fuel_reports.fuel_entries_by_vehicle_title') }}
                                </div>
                                <div class="kt-notification__item-time">
                                    {{ __('reports.fuel_reports.fuel_entries_by_vehicle_subtitle') }}
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            {{ __('reports.issues_reports.heading') }}
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="kt-notification kt-notification--fit">
                        <a href="#" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                -
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-transform-c">
                                    {{ __('reports.issues_reports.issues_list_title') }}
                                </div>
                                <div class="kt-notification__item-time">
                                    {{ __('reports.issues_reports.issues_list_subtitle') }}
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            {{ __('reports.service_reports.heading') }}
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="kt-notification kt-notification--fit">
                        <a href="#" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                -
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-transform-c">
                                    {{ __('reports.service_reports.service_summary_report_title') }}
                                </div>
                                <div class="kt-notification__item-time">
                                    {{ __('reports.service_reports.service_summary_report_subtitle') }}
                                </div>
                            </div>
                        </a>

                        <a href="#" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                -
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-transform-c">
                                    {{ __('reports.service_reports.service_history_report_title') }}
                                </div>
                                <div class="kt-notification__item-time">
                                    {{ __('reports.service_reports.service_history_report_subtitle') }}
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection