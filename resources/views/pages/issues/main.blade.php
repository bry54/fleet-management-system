@extends('layouts.master')

@section('page-title', __('issues.menu_title') )

@section('page-header-title', __('issues.page_header_title') )

@section('header-left')
    @include('pages.issues.header-left')
@endsection

@section('header-right')
    @include('pages.issues.header-right')
@endsection

@section('content')
    @yield('sub-page')
@endsection