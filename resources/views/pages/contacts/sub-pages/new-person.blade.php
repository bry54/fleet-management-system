@extends('pages.contacts.main')

@section('additional-items')

@endsection

@section('sub-page')
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="flaticon2-writing"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    {{ __('contacts.people_contacts.new_contact')}}
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">

            </div>
        </div>
        <div class="kt-portlet__body">
            Form inputs here
        </div>
        <div class="kt-portlet__foot">
            <div class="row align-items-right">
                <div class="col-lg-12 kt-align-right">
                    <button type="submit" class="btn btn-brand btn-sm">{{ __('global.common_labels.save')}}</button>
                    <button type="button" class="btn btn-default btn-sm">{{ __('global.common_labels.cancel')}}</button>
                </div>
            </div>
        </div>
    </div>
@endsection
