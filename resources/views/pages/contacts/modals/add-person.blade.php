<!--begin::Modal-->
<div class="modal fade" id="add_person" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel">New Person </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <!--begin::Accordion-->
                <div class="accordion  accordion-toggle-arrow" id="contactInfoAccordion">
                    <div class="card">
                        <div class="card-header" id="headingOne4">
                            <div class="card-title" data-toggle="collapse" data-target="#collapseOne4" aria-expanded="true" aria-controls="collapseOne4">
                                <i class="flaticon2-shield"></i> Personal Details
                            </div>
                        </div>
                        <div id="collapseOne4" class="collapse show" aria-labelledby="headingOne" data-parent="#contactInfoAccordion">
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label">Prefix:</label>
                                    <div class="col-lg-4">
                                        <input type="email" class="form-control" placeholder="Prefix">
                                    </div>
                                    <label class="col-lg-2 col-form-label">First Name:</label>
                                    <div class="col-lg-4">
                                        <input type="email" class="form-control" placeholder="First name">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label">Middle Name:</label>
                                    <div class="col-lg-4">
                                        <input type="email" class="form-control" placeholder="Middle name">
                                    </div>
                                    <label class="col-lg-2 col-form-label">Last Name:</label>
                                    <div class="col-lg-4">
                                        <input type="email" class="form-control" placeholder="Last name">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo4">
                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseTwo4" aria-expanded="false" aria-controls="collapseTwo4">
                                <i class="flaticon2-phone"></i> Contact Information
                            </div>
                        </div>
                        <div id="collapseTwo4" class="collapse" aria-labelledby="headingTwo1" data-parent="#contactInfoAccordion">
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label">Personal email:</label>
                                    <div class="col-lg-4">
                                        <input type="email" class="form-control" placeholder="Personal Email">
                                    </div>
                                    <label class="col-lg-2 col-form-label">Work email:</label>
                                    <div class="col-lg-4">
                                        <input type="email" class="form-control" placeholder="Work Email">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label">Personal Phone:</label>
                                    <div class="col-lg-4">
                                        <input type="phone" class="form-control" placeholder="Personal Phone">
                                    </div>
                                    <label class="col-lg-2 col-form-label">Work Phone:</label>
                                    <div class="col-lg-4">
                                        <input type="phone" class="form-control" placeholder="Work Phone">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label">Contact Address:</label>
                                    <div class="col-lg-8">
                                        <textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Accordion-->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save Contact</button>
            </div>
        </div>
    </div>
</div>
<!--end::Modal-->