<!--begin::Modal-->
<div class="modal fade" id="add_company" tabindex="-1" role="dialog" aria-labelledby="addCompanyModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addCompanyModalLabel">New Company </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <!--begin::Accordion-->
                <div class="accordion  accordion-toggle-arrow" id="companyContactInfoAccordion">
                    <div class="card">
                        <div class="card-header" id="headingOne4">
                            <div class="card-title" data-toggle="collapse" data-target="#collapseContactInfo" aria-expanded="true" aria-controls="collapseContactInfo">
                                <i class="flaticon2-shield"></i> Organization and Contact Information
                            </div>
                        </div>
                        <div id="collapseContactInfo" class="collapse show" aria-labelledby="headingOne4" data-parent="#companyContactInfoAccordion">
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label">Organization Name:</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" placeholder="Organization Name">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label">Contact Address:</label>
                                    <div class="col-lg-8">
                                        <textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label">Main Phone:</label>
                                    <div class="col-lg-4">
                                        <input type="phone" class="form-control" placeholder="Main Phone">
                                    </div>
                                    <label class="col-lg-2 col-form-label">Other Phone:</label>
                                    <div class="col-lg-4">
                                        <input type="phone" class="form-control" placeholder="Other Phone">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label">Email Address:</label>
                                    <div class="col-lg-4">
                                        <input type="email" class="form-control" placeholder="Email Address">
                                    </div>
                                    <label class="col-lg-2 col-form-label">Website:</label>
                                    <div class="col-lg-4">
                                        <input type="url" class="form-control" placeholder="Website">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo4">
                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseEmployeeInfo" aria-expanded="false" aria-controls="collapseEmployeeInfo">
                                <i class="flaticon2-avatar"></i> Employees
                            </div>
                        </div>
                        <div id="collapseEmployeeInfo" class="collapse" aria-labelledby="headingTwo4" data-parent="#companyContactInfoAccordion">
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Contact Name:</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" placeholder="Contact Name">
                                    </div>
                                    <div class="col-lg-3">
                                        <button class="btn btn-sm btn-success ">Add</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Accordion-->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save Contact</button>
            </div>
        </div>
    </div>
</div>
<!--end::Modal-->