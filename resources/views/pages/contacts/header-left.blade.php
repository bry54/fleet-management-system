<div class="kt-subheader__main">
    @if(request()->route()->getName() !== 'contacts-main')
        <a href="{{ route('contacts-main') }}" class="btn btn-outline-dark btn-bold btn-sm btn-icon-h">
            <i class="fa fa-home"></i>
            <span class="kt-hidden-mobile">{{__('contacts.btn_home')}}</span>
        </a>
        <div style="margin: 0 10px;"></div>
        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
    @endif

    {{--<h3 class="kt-subheader__title">{{ __('contacts.menu_title') }}</h3>--}}

    <a href="{{url('contacts')}}"
       class="btn btn-outline-info {{ (request()->is('contacts') || request()->is('contacts')) ? 'active' : '' }}">
        {{__('contacts.btn_all')}}
    </a>

    <a href="{{url('contacts/companies')}}"
       class="btn btn-outline-info {{ (request()->is('contacts/companies') || request()->is('contacts/companies')) ? 'active' : '' }}">
        {{__('contacts.btn_companies')}}
    </a>

    <a href="{{url('contacts/people')}}"
       class="btn btn-outline-info {{ request()->is('contacts/people') || request()->is('contacts/people') ? 'active' : '' }}">
        {{__('contacts.btn_people')}}
    </a>
</div>