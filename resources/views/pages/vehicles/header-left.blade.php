<div class="kt-subheader__main">
    @if(request()->route()->getName() !== 'vehicles-main')
        <a href="{{ route('vehicles-main') }}" class="btn btn-outline-dark btn-bold btn-sm btn-icon-h">
            <i class="fa fa-home"></i>
            <span class="kt-hidden-mobile">{{__('vehicles.btn_home')}}</span>
        </a>
        <div style="margin: 0 10px;"></div>
        <span class="kt-subheader__separator kt-subheader__separator--v"></span>
    @endif

    <a href="{{url('vehicles')}}"
       class="btn btn-outline-info {{ (request()->is('vehicles') || request()->is('vehicles/new-vehicle')) ? 'active' : '' }}">
        All Vehicles
    </a>

    <a href="{{url('vehicles/vehicle-assignments')}}"
       class="btn btn-outline-info {{ request()->is('vehicles/vehicle-assignments') || request()->is('vehicles/new-vehicle-assignment') ? 'active' : '' }}">
        Vehicle Assignment
    </a>
</div>
