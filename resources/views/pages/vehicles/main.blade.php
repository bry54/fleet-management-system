@extends('layouts.master')

@section('page-title', __('vehicles.menu_title') )

@section('page-header-title', __('vehicles.page_header_title') )

@section('header-left')
    @include('pages.vehicles.header-left')
@endsection

@section('header-right')
    @include('pages.vehicles.header-right')
@endsection

@section('content')
    @yield('sub-page')
@endsection
