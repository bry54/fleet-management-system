@extends('pages.vehicles.main')

@section('additional-items')

@endsection

@section('sub-page')
    <div id="unassign-vehicle">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="flaticon2-writing"></i>
                </span>
                    <h3 class="kt-portlet__head-title">
                        Vehicle Unassignment
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">

                </div>
            </div>
            <div class="kt-portlet__body">
                Form inputs here
            </div>
            <div class="kt-portlet__foot">
                <div class="row align-items-right">
                    <div class="col-lg-12 kt-align-right">
                        <button type="submit" class="btn btn-danger btn-sm">Unassign</button>
                        <button type="button" class="btn btn-default btn-sm">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        let app = new Vue({
            el: '#unassign-vehicle',
            data: {
                vehicle: null
            },

            created: function () {

            },

            mounted: function () {

            },

            computed: {

            },

            watch: {

            },

            methods: {

            }


        })
    </script>
@endsection
