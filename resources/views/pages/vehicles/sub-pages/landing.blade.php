@extends('pages.vehicles.main')

@section('sub-page')
    <div class="kt-portlet kt-portlet--mobile" id="vehicle-landing">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-google-drive-file"></i>
										</span>
                <h3 class="kt-portlet__head-title">
                    {{ __('vehicles.menu_title') }}
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="{{ url('vehicles/new-vehicle') }}" class="btn btn-default btn-icon-sm">
                            <i class="la la-plus"></i> {{ __('New Vehicle') }}
                        </a>
                        <div class="dropdown dropdown-inline" v-if="vehicles && vehicles.length">
                            <button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="la la-download"></i> {{ __('global.common_labels.export') }}
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <ul class="kt-nav">
                                    <li class="kt-nav__section kt-nav__section--first">
                                        <span class="kt-nav__section-text">{{ __('global.common_labels.choose') }}</span>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-print"></i>
                                            <span class="kt-nav__link-text">{{ __('global.common_labels.print') }}</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-file-excel-o"></i>
                                            <span class="kt-nav__link-text">{{ __('global.common_labels.excel') }}</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1" v-if="vehicles && vehicles.length">
                <thead>
                <tr>
                    <th width="20%">{{ __('vehicles.tbl_name') }}</th>
                    <th width="10%">{{ __('vehicles.tbl_status') }}</th>
                    <th width="10%">{{ __('vehicles.tbl_type') }}</th>
                    <th width="15%">{{ __('vehicles.tbl_last_recorded_meter') }}</th>
                    <th width="15%">{{ __('vehicles.tbl_group') }}</th>
                    <th width="20%">{{ __('vehicles.tbl_assignee') }}</th>
                    <th width="15%">{{ __('global.common_labels.actions') }}</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="vehicle in vehicles">
                    <td>
                        <h6 class="form-text kt-font-transform-u">@{{ vehicle.name }}</h6>
                        <span class="form-text text-black-50">Licence Plate: <span class="kt-font-transform-u text-muted">@{{ vehicle.plate }}</span></span>
                    </td>
                    <td>
                        <span class="form-text kt-badge kt-badge--success kt-badge--inline kt-badge--pill kt-badge--rounded kt-font-transform-u font-weight-bold">@{{ vehicle.status }}</span>
                    </td>
                    <td>
                        <span class="form-text"> @{{ vehicle.type }} </span>
                    </td>
                    <td>
                        <span class="form-text kt-font-bold">@{{ vehicle.meter }}</span>
                        <span class="form-text"><abbr title="{{date('d M Y H:m')}}">@{{ vehicle.last_check }}</abbr></span>
                    </td>
                    <td>
                        <span class="form-text">@{{ vehicle.group }}</span>
                    </td>
                    <td>
                        <span class="form-text">@{{ vehicle.operator }}</span>
                        <a href="{{ url('/vehicles/vehicle-un-assignment') }}" class="form-text kt-link kt-link--state kt-link--danger font-weight-bold kt-font-transform-u" v-if="vehicle.operator">Unassign</a>
                        <a href="{{ url('/vehicles/new-vehicle-assignment') }}" class="form-text kt-link kt-link--state kt-link--success font-weight-bold kt-font-transform-u" v-else= href="#">Assign</a>
                    </td>
                    <td data-field="Actions" data-autohide-disabled="false" class="kt-datatable__cell">
                        <span style="overflow: visible; position: relative; width: 110px;">
                            <div class="dropdown">
                                <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                                    <i class="la la-edit"></i>
                                </a>

                                <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                                    <i class="la la-trash"></i>
                                </a>
                            </div>
                        </span>
                    </td>
                </tr>
                </tbody>
            </table>
            <!--end: Datatable -->

            <div v-else class="text-center">
                <h3 class="display-4">No vehicle entries found. Please start by adding vehicles</h3>

                <a href="{{ url('vehicles/new-vehicle') }}" class="btn btn-primary btn-upper kt-margin-50">CREATE VEHICLE ENTRY</a>
            </div>
        </div>
    </div>

    <script>
        let app = new Vue({
            el: '#vehicle-landing',
            data: {
                vehicles: [{
                    name: 'Land Rover Defender',
                    type: 'Pick Up',
                    plate: 'AS-1234-HRE',
                    status: 'Available',
                    meter: '20,000 KM',
                    last_check: 'Yesterday',
                    group: 'Sales',
                    operator: 'Kudakwashe Mandere',
                }, {
                    name: 'Chevrolet Express Cargo',
                    type: 'Van',
                    plate: 'AS-0033-BYO',
                    status: 'Available',
                    meter: '10,000 KM',
                    last_check: 'Last Month',
                    group: 'Distribution',
                    operator: 'Paidamoyo Mandebvu',
                }, {
                    name: 'Freightliner Cascadia',
                    type: 'Semi Truck',
                    plate: 'AS-1133-BYO',
                    status: 'Available',
                    meter: '50,000 KM',
                    last_check: 'Last Week',
                    group: 'Heath Aid',
                    operator: null,
                }]
            },

            created: function () {

            },

            mounted: function () {

            },

            computed: {

            },

            watch: {

            },

            methods: {

            }


        })
    </script>

@endsection
