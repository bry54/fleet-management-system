@extends('pages.vehicles.main')

@section('sub-page')
    <div class="kt-portlet kt-portlet--mobile" id="vehicle-assignments">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-google-drive-file"></i>
										</span>
                <h3 class="kt-portlet__head-title">
                    {{ __('Vehicle Assignments') }}
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="{{ url('vehicles/new-vehicle') }}" class="btn btn-default btn-icon-sm">
                            <i class="la la-plus"></i> {{ __('New Vehicle Assignment') }}
                        </a>
                        <div class="dropdown dropdown-inline" v-if="vehicles && vehicles.length">
                            <button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="la la-download"></i> {{ __('global.common_labels.export') }}
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <ul class="kt-nav">
                                    <li class="kt-nav__section kt-nav__section--first">
                                        <span class="kt-nav__section-text">{{ __('global.common_labels.choose') }}</span>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-print"></i>
                                            <span class="kt-nav__link-text">{{ __('global.common_labels.print') }}</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-file-excel-o"></i>
                                            <span class="kt-nav__link-text">{{ __('global.common_labels.excel') }}</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1" v-if="vehicles && vehicles.length">
                <thead>
                <tr>
                    <th>{{ __('vehicles.tbl_matter') }}</th>
                    <th>{{ __('vehicles.tbl_client') }}</th>
                    <th>{{ __('vehicles.tbl_responsible_solicitor') }}</th>
                    <th>{{ __('vehicles.tbl_originating_solicitor') }}</th>
                    <th>{{ __('vehicles.tbl_practise_area') }}</th>
                    <th>{{ __('vehicles.tbl_open_date') }}</th>
                    <th>{{ __('global.common_labels.actions') }}</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>1</td>
                    <td>61715-075</td>
                    <td>China</td>
                    <td>Tieba</td>
                    <td>746 Pine View Junction</td>
                    <td>Nixie Sailor</td>
                    <td data-field="Actions" data-autohide-disabled="false" class="kt-datatable__cell">
                        <span style="overflow: visible; position: relative; width: 110px;">
                            <div class="dropdown">
                                <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                                    <i class="la la-edit"></i>
                                </a>

                                <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                                    <i class="la la-trash"></i>
                                </a>
                            </div>
                        </span>
                    </td>
                </tr>
                </tbody>
            </table>
            <!--end: Datatable -->

            <div v-else class="text-center">
                <h3 class="display-4">No vehicle assignment entries found. Please start by adding vehicles</h3>

                <a href="{{ url('vehicles/new-vehicle-assignment') }}" class="btn btn-primary btn-upper kt-margin-50">CREATE VEHICLE ASSIGNMENT ENTRY</a>
            </div>
        </div>
    </div>

    <script>
        let app = new Vue({
            el: '#vehicle-assignments',
            data: {
                vehicles: null
            },

            created: function () {

            },

            mounted: function () {

            },

            computed: {

            },

            watch: {

            },

            methods: {

            }
        })
    </script>

@endsection
