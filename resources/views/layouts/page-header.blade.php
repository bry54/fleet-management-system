<div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
    <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default ">
        <ul class="kt-menu__nav ">
            <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel kt-menu__item--active" data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                <h5 style="margin-top: 20px;" class="text-uppercase text-black-50">
                    @yield('page-header-title')
                </h5>
            </li>
        </ul>
    </div>
</div>

<div class="kt-header__topbar">
    <!--begin: Quick Actions -->
    <div class="kt-header__topbar-item dropdown">
        <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="30px,0px" aria-expanded="true">
									<span class="kt-header__topbar-icon kt-pulse kt-pulse--brand">
										<svg xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                             viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<rect x="0" y="0" width="24" height="24"/>
												<path d="M2.56066017,10.6819805 L4.68198052,8.56066017 C5.26776695,7.97487373 6.21751442,7.97487373 6.80330086,8.56066017 L8.9246212,10.6819805 C9.51040764,11.267767 9.51040764,12.2175144 8.9246212,12.8033009 L6.80330086,14.9246212 C6.21751442,15.5104076 5.26776695,15.5104076 4.68198052,14.9246212 L2.56066017,12.8033009 C1.97487373,12.2175144 1.97487373,11.267767 2.56066017,10.6819805 Z M14.5606602,10.6819805 L16.6819805,8.56066017 C17.267767,7.97487373 18.2175144,7.97487373 18.8033009,8.56066017 L20.9246212,10.6819805 C21.5104076,11.267767 21.5104076,12.2175144 20.9246212,12.8033009 L18.8033009,14.9246212 C18.2175144,15.5104076 17.267767,15.5104076 16.6819805,14.9246212 L14.5606602,12.8033009 C13.9748737,12.2175144 13.9748737,11.267767 14.5606602,10.6819805 Z"
                                                      fill="#000000" opacity="0.3"/>
												<path d="M8.56066017,16.6819805 L10.6819805,14.5606602 C11.267767,13.9748737 12.2175144,13.9748737 12.8033009,14.5606602 L14.9246212,16.6819805 C15.5104076,17.267767 15.5104076,18.2175144 14.9246212,18.8033009 L12.8033009,20.9246212 C12.2175144,21.5104076 11.267767,21.5104076 10.6819805,20.9246212 L8.56066017,18.8033009 C7.97487373,18.2175144 7.97487373,17.267767 8.56066017,16.6819805 Z M8.56066017,4.68198052 L10.6819805,2.56066017 C11.267767,1.97487373 12.2175144,1.97487373 12.8033009,2.56066017 L14.9246212,4.68198052 C15.5104076,5.26776695 15.5104076,6.21751442 14.9246212,6.80330086 L12.8033009,8.9246212 C12.2175144,9.51040764 11.267767,9.51040764 10.6819805,8.9246212 L8.56066017,6.80330086 C7.97487373,6.21751442 7.97487373,5.26776695 8.56066017,4.68198052 Z"
                                                      fill="#000000"/>
											</g>
										</svg> <span class="kt-pulse__ring"></span>
									</span>
        </div>
        <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">
            <form>

                <!--begin: Head -->
                <div class="kt-head kt-head--skin-dark" style="background-image: url({{URL::asset('assets/media/misc/bg-1.jpg')}})">
                    <h3 class="kt-head__title">
                        {{ __('global.actions.title') }}
                        <span class="kt-space-15"></span>
                    </h3>
                </div>

                <!--end: Head -->

                <!--begin: Grid Nav -->
                <div class="kt-grid-nav kt-grid-nav--skin-light">
                    <div class="kt-grid-nav__row">
                        <a href="{{ url('vehicles/new-vehicle') }}" class="kt-grid-nav__item">
													<span class="kt-grid-nav__icon">
														<svg xmlns="http://www.w3.org/2000/svg"
                                                             xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                             height="24px" viewBox="0 0 24 24" version="1.1"
                                                             class="kt-svg-icon kt-svg-icon--success kt-svg-icon--lg">
															<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"/>
                                                                <polygon fill="#000000" opacity="0.3" points="6 4 18 4 20 6.5 4 6.5"/>
                                                                <path d="M5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L20.999994,17.0000172 C20.999994,18.1045834 20.1045662,19.0000112 19,19.0000112 C17.4805018,19.0000037 16.4805051,19 16,19 C15,19 14.5,17 12,17 C9.5,17 9.5,19 8,19 C7.31386312,19 6.31387037,19.0000034 5.00002173,19.0000102 L5.00002173,19.0000216 C3.89544593,19.0000273 3.00000569,18.1045963 3,17.0000205 C3,17.000017 3,17.0000136 3,17.0000102 L3,8 C3,6.8954305 3.8954305,6 5,6 Z M8,14 C9.1045695,14 10,13.1045695 10,12 C10,10.8954305 9.1045695,10 8,10 C6.8954305,10 6,10.8954305 6,12 C6,13.1045695 6.8954305,14 8,14 Z M16,14 C17.1045695,14 18,13.1045695 18,12 C18,10.8954305 17.1045695,10 16,10 C14.8954305,10 14,10.8954305 14,12 C14,13.1045695 14.8954305,14 16,14 Z" fill="#000000"/>
                                                            </g>
														</svg> </span>
                            <span class="kt-grid-nav__title">{{ __('global.actions.vehicle_title') }}</span>
                            <span class="kt-grid-nav__desc">{{ __('global.actions.vehicle_sub') }}</span>
                        </a>

                        <a href="{{ url('fuel/new-fuel-entry') }}" class="kt-grid-nav__item">
													<span class="kt-grid-nav__icon">
														<svg xmlns="http://www.w3.org/2000/svg"
                                                             xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                             height="24px" viewBox="0 0 24 24" version="1.1"
                                                             class="kt-svg-icon kt-svg-icon--success kt-svg-icon--lg">
															<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"/>
                                                                <rect fill="#000000" opacity="0.3" transform="translate(12.023129, 11.752757) rotate(33.000000) translate(-12.023129, -11.752757) " x="11.0231287" y="7.92751491" width="2" height="7.65048405" rx="1"/>
                                                                <path d="M7.5,3 L16.5,3 C18.9852814,3 21,5.01471863 21,7.5 L21,15.5 C21,17.9852814 18.9852814,20 16.5,20 L7.5,20 C5.01471863,20 3,17.9852814 3,15.5 L3,7.5 C3,5.01471863 5.01471863,3 7.5,3 Z M6,7.88235294 L8.57142857,12 L15.4285714,12 L18,7.88235294 C17,5.96078431 15,5 12,5 C9,5 7,5.96078431 6,7.88235294 Z" fill="#000000"/>
                                                            </g>
														</svg> </span>
                            <span class="kt-grid-nav__title">{{ __('global.actions.fuel_title') }}</span>
                            <span class="kt-grid-nav__desc">{{ __('global.actions.fuel_sub') }}</span>
                        </a>

                        <a href="{{ url('issues/new-issue') }}" class="kt-grid-nav__item">
													<span class="kt-grid-nav__icon">
														<svg xmlns="http://www.w3.org/2000/svg"
                                                             xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                             height="24px" viewBox="0 0 24 24" version="1.1"
                                                             class="kt-svg-icon kt-svg-icon--success kt-svg-icon--lg">
															<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"/>
                                                                <path d="M2,13.1500272 L2,5.5 C2,4.67157288 2.67157288,4 3.5,4 L6.37867966,4 C6.77650439,4 7.15803526,4.15803526 7.43933983,4.43933983 L10,7 L20.5,7 C21.3284271,7 22,7.67157288 22,8.5 L22,19.5 C22,20.3284271 21.3284271,21 20.5,21 L10.9835977,21 C10.9944753,20.8347382 11,20.6680143 11,20.5 C11,16.3578644 7.64213562,13 3.5,13 C2.98630124,13 2.48466491,13.0516454 2,13.1500272 Z" fill="#000000"/>
                                                                <path d="M4.5,16 C5.05228475,16 5.5,16.4477153 5.5,17 L5.5,19 C5.5,19.5522847 5.05228475,20 4.5,20 C3.94771525,20 3.5,19.5522847 3.5,19 L3.5,17 C3.5,16.4477153 3.94771525,16 4.5,16 Z M4.5,23 C3.94771525,23 3.5,22.5522847 3.5,22 C3.5,21.4477153 3.94771525,21 4.5,21 C5.05228475,21 5.5,21.4477153 5.5,22 C5.5,22.5522847 5.05228475,23 4.5,23 Z" fill="#000000" opacity="0.3"/>
                                                            </g>
														</svg> </span>
                            <span class="kt-grid-nav__title">{{ __('global.actions.issue_title') }}</span>
                            <span class="kt-grid-nav__desc">{{ __('global.actions.issue_sub') }}</span>
                        </a>
                    </div>
                </div>
                <!--end: Grid Nav -->
            </form>
        </div>
    </div>
    <!--end: Quick Actions -->

    <!--begin: Language bar -->
    <div class="kt-header__topbar-item kt-header__topbar-item--langs">
        <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
									<span class="kt-header__topbar-icon">
										@if (Config::get('app.locale') == 'en') <img class="" src="{{ asset('assets/media/flags/263-zimbabwe.svg') }}" alt=""/>@endif
                                        {{--@if (Config::get('app.locale') == 'tr') <img class="" src="{{ asset('assets/media/flags/006-turkey.svg') }}" alt=""/>@endif
                                        @if (Config::get('app.locale') == 'el') <img class="" src="{{ asset('assets/media/flags/021-greece.svg') }}" alt=""/>@endif--}}
									</span>
        </div>
        <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround">
            <ul class="kt-nav kt-margin-t-10 kt-margin-b-10">
                <li class="kt-nav__item {{ Config::get('app.locale') == 'en' ? 'kt-nav__item--active' : ''}} ">
                    <a href="{{ url('change-language/en')}} " class="kt-nav__link">
                        <span class="kt-nav__link-icon"><img src="{{ asset('assets/media/flags/263-zimbabwe.svg') }}"
                                                             alt=""/></span>
                        <span class="kt-nav__link-text">{{ __('global.lang.english') }}</span>
                    </a>
                </li>
                {{--<li class="kt-nav__item" {{ Config::get('app.locale') == 'tr' ? 'kt-nav__item--active' : ''}}>
                    <a href=" {{url('change-language/tr')}} " class="kt-nav__link">
                        <span class="kt-nav__link-icon"><img src="{{ asset('assets/media/flags/006-turkey.svg') }}"
                                                             alt=""/></span>
                        <span class="kt-nav__link-text">{{ __('global.lang.turkish') }}</span>
                    </a>
                </li>
                <li class="kt-nav__item" {{ Config::get('app.locale') == 'el' ? 'kt-nav__item--active' : ''}}>
                    <a href=" {{url('change-language/el')}} " class="kt-nav__link">
                        <span class="kt-nav__link-icon"><img src="{{ asset('assets/media/flags/021-greece.svg') }}"
                                                             alt=""/></span>
                        <span class="kt-nav__link-text">{{ __('global.lang.greek') }}</span>
                    </a>
                </li>--}}
            </ul>
        </div>
    </div>
    <!--end: Language bar -->

    <!--begin: User Bar -->
    <div class="kt-header__topbar-item kt-header__topbar-item--user">
        <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
            <div class="kt-header__topbar-user">
                <span class="kt-header__topbar-welcome kt-hidden-mobile">{{ __('global.user.hie') }}</span>
                <span class="kt-header__topbar-username kt-hidden-mobile">{{ Auth::user()->firstname }}</span>

                <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold">{{ substr(Auth::user()->firstname, 0, 1) }}</span>
            </div>
        </div>
        <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">

            <!--begin: Head -->
            <div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x"
                 style="background-image: url( {{ asset('assets/media/misc/bg-1.jpg') }})">
                <div class="kt-user-card__avatar">
                    <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                    <span class="kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success">{{ substr(Auth::user()->firstname, 0, 1) }}</span>
                </div>
                <div class="kt-user-card__name">
                    {{ Auth::user()->title .' '. Auth::user()->firstname .' '. Auth::user()->lastname }}
                </div>
            </div>
            <!--end: Head -->

            <!--begin: Navigation -->
            <div class="kt-notification">
                <a href="#" class="kt-notification__item">
                    <div class="kt-notification__item-icon">
                        <i class="flaticon2-calendar-3 kt-font-success"></i>
                    </div>
                    <div class="kt-notification__item-details">
                        <div class="kt-notification__item-title kt-font-bold">
                            {{ __('global.user.profile_title') }}
                        </div>
                        <div class="kt-notification__item-time">
                            {{ __('global.user.profile_sub') }}
                        </div>
                    </div>
                </a>
                <a href="#" class="kt-notification__item">
                    <div class="kt-notification__item-icon">
                        <i class="flaticon2-mail kt-font-warning"></i>
                    </div>
                    <div class="kt-notification__item-details">
                        <div class="kt-notification__item-title kt-font-bold">
                            {{ __('global.user.message_title') }}
                        </div>
                        <div class="kt-notification__item-time">
                            {{ __('global.user.message_sub') }}
                        </div>
                    </div>
                </a>
                <div class="kt-notification__custom kt-space-between">
                    <form method="POST" action="{{ url('/logout') }}">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-label btn-label-brand btn-sm btn-bold">{{ __('global.user.sign_out') }}</button>
                    </form>
                </div>
            </div>
            <!--end: Navigation -->
        </div>
    </div>
    <!--end: User Bar -->
</div>