@extends('auth.layouts.master')

@section('form-title', 'Recover Password')

@section('auth-form')
    <form class="kt-form" id="kt_login_form" method="POST" action="{{ route('password.update') }}">
        @csrf
        <div class="form-group">
            <input class="form-control" type="text" placeholder="Email" name="email" id="kt_email" autocomplete="off">
        </div>

        <!--begin::Action-->
        <div class="kt-login__actions">
            <a href="{{ url('/login') }}" class="kt-link kt-login__link-forgot">
                Want to Login ?
            </a>
            <button
                    type="submit"
                    id="kt_login_signin_submit"
                    class="btn btn-primary btn-elevate kt-login__btn-primary">
                Recover Password
            </button>
        </div>
    </form>
@endsection