import {createStore, combineReducers, applyMiddleware, compose} from "redux";
import thunk from 'redux-thunk';

import {reducer as globalReducer, initialState as initialGlobalState} from '../../store/reducers/global'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
    globalReducer: globalReducer,
});

const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));

export { store };