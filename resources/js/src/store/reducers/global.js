import {SET_CONTENT_HEADER_ITEMS} from "../utilities/actionTypes";

const initialState = {
    contentHeader:{
        leftItems: {
            title: null,
            buttons: []
        },
        rightItems: {
            buttons: []
        },
    },
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_CONTENT_HEADER_ITEMS:{
            const headerElement = action.payload;

            return {
                ...state,
                contentHeader: {
                    leftItems: headerElement.leftItems,
                    rightItems: headerElement.rightItems
                }
            };
        }

        default:
            return state;
    }
};

export {initialState, reducer};
