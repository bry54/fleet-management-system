import {SET_CONTENT_HEADER_ITEMS} from "../utilities/actionTypes";

export const setHeaderContent = (leftElements, rightElements) => async dispatch => {
    dispatch({
        type: SET_CONTENT_HEADER_ITEMS,
        payload: {
            leftItems: leftElements,
            rightItems: rightElements
        }
    })
};