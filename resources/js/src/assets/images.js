export const LOGO = require('../../../../public/assets/media/logos/logo-light.png');
export const TR_FLAG = require('../../../../public/assets/media/flags/006-turkey.svg');
export const EN_FLAG = require('../../../../public/assets/media/flags/020-flag.svg');
export const GR_FLAG = require('../../../../public/assets/media/flags/019-france.svg');

export const BG_1 = require('../../../../public/assets/media/misc/bg-1.jpg');
