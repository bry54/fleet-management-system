import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'

import {BrowserRouter, Link, Route, Switch} from 'react-router-dom'
import {store} from "../store/utilities/storeConfiguration";
import BodyWrapper from "../sections/BodyWrapper";
import SideMenuSection from "../sections/side-menu/SideMenuSection";

class MainApplication extends Component {
    render () {
        return (
            <Provider store={store}>
                <BrowserRouter>
                    <SideMenuSection/>
                    <BodyWrapper/>
                </BrowserRouter>
            </Provider>
        )
    }
}

ReactDOM.render(<MainApplication />, document.getElementById('app'));
