import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import ContentHeaderSection from "./content-header/ContentHeaderSection";
import ContentAreaSection from "./content-area/ContentAreaSection";

export default class ContentHolder extends Component {
    componentDidMount () {

    }

    render () {
        return (
                <div className="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
                    <ContentHeaderSection/>

                    <ContentAreaSection/>
                </div>
        )
    }
}

const styles = {

};
