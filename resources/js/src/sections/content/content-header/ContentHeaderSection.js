import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class ContentHeaderSection extends Component {
    componentDidMount () {

    }

    render () {
        return (
                <div className="kt-subheader  kt-grid__item" id="kt_subheader">
                    <div className="kt-container  kt-container--fluid ">
                        <div className="kt-subheader__main">
                            <h3 className="kt-subheader__title">Some Left Title</h3>
                            <span className="kt-subheader__separator kt-subheader__separator--v"></span>
                            <a className="btn btn-label-info btn-bold btn-sm btn-icon-h kt-margin-l-10 active">
                                Some Left Button
                            </a>
                        </div>

                        <div className="kt-subheader__toolbar">
                            <div className="kt-subheader__wrapper">
                                <a className="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10 {{ request()->route()->getName() ==='new-company-contact'? 'active' : '' }}">
                                    Some Right Button
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
        )
    }
}

const styles = {

};
