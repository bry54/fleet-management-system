import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class FooterSection extends Component {
    componentDidMount () {

    }

    render () {
        return (
                <div className="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
                    <div className="kt-container  kt-container--fluid ">
                        <div className="kt-footer__copyright">
                            2020 @ Company Name
                        </div>
                        <div className="kt-footer__menu">
                            <a target="_blank" className="kt-footer__menu-link kt-link">About</a>
                            <a target="_blank" className="kt-footer__menu-link kt-link">Team</a>
                            <a target="_blank" className="kt-footer__menu-link kt-link">Help</a>
                        </div>
                    </div>
                </div>
        )
    }
}

const styles = {
    footerContainer: {

    },

    infoContainer:{

    },

    copyright:{

    },

    links:{

    }
};
