import axios from 'axios'
import React, { Component } from 'react'
import PageHeader from "./page-header/PageHeaderSection";
import ContentHolder from "./content/ContentHolder";
import FooterSection from "./footer/FooterSection";

export default class BodyWrapper extends Component {
    componentDidMount () {

    }

    render () {
        return (
                <div className="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
                    <PageHeader/>

                    <ContentHolder/>

                    <FooterSection/>
                </div>
        )
    }
}

const styles = {

};
