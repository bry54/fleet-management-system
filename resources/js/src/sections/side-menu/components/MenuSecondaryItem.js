import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {Link} from "react-router-dom";
import MenuPrimaryItem from "./MenuPrimaryItem";

export default class MenuSecondaryItem extends Component{

    render() {
        const menuItem  = this.props.item;
        const index  = this.props.index;
        console.log(menuItem, index)
        if (!menuItem.children) {
            return ( <li className="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                    <Link className="kt-menu__link kt-menu__toggle" to={menuItem.uri}>
                        <i className="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span className="kt-menu__link-text">{menuItem.title}</span>
                    </Link>
                </li>
            )
        } else {
            return (
                <li className="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                    <Link className="kt-menu__link kt-menu__toggle" to={menuItem.uri}>
                        <i className="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                        <span className="kt-menu__link-text">{menuItem.title}</span>
                        <i className="kt-menu__ver-arrow la la-angle-right"></i>
                    </Link>
                    <div className="kt-menu__submenu "><span className="kt-menu__arrow"></span>
                        <ul className="kt-menu__subnav">
                            { menuItem.children.map( (child, index) => (
                                <MenuSecondaryItem key={JSON.stringify(child)} item = {child} index={index}/>
                            ))}
                        </ul>
                    </div>
                </li>
            )
        }
    }
}
