import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {Link} from "react-router-dom";
import MenuSecondaryItem from "./MenuSecondaryItem";

export default class MenuPrimaryItem extends Component{
    render() {
        const menuItem  = this.props.item;

        if (menuItem.children && menuItem.children.length) {
            return (
                <li className="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                    <Link className="kt-menu__link kt-menu__toggle" to={menuItem.uri}>
                        <span className="kt-menu__link-icon">{menuItem.svgPath}</span>
                        <span className="kt-menu__link-text">{menuItem.title}</span>
                        <i className="kt-menu__ver-arrow la la-angle-right"></i>
                    </Link>

                    <div className="kt-menu__submenu " kt-hidden-height="240" style={{display: 'none', overflow: 'hidden'}}>
                        <span className="kt-menu__arrow"></span>
                        <ul className="kt-menu__subnav">
                            <li className="kt-menu__item  kt-menu__item--parent" aria-haspopup="true">
                                <span className="kt-menu__link">
                                    <span className="kt-menu__link-text">{menuItem.title}</span>
                                </span>
                            </li>

                            { menuItem.children.map((child, index) => (
                                <MenuSecondaryItem key={JSON.stringify(child)} item={child} index={index}/>
                            ))}
                        </ul>
                    </div>
                </li> )
        } else {
            return (
                <li className="kt-menu__item" aria-haspopup="true">
                    <Link to={menuItem.uri} className="kt-menu__link ">
                        <span className="kt-menu__link-icon"> {menuItem.svgPath}</span>
                        <span className="kt-menu__link-text">{menuItem.title}</span>
                    </Link>
                </li>
            )
        }
    }

};


