<?php

return [
    'menu_title' => 'Reports',
    'page_header_title' => 'Reports Management',
    'btn_home' => 'Reports Home',

    'vehicle_reports' => [
        'heading' => 'Vehicle reports',

        'vehicle_list_title' => 'Vehicle List',
        'vehicle_list_subtitle' => 'some information about report',

        'vehicle_details_title' => 'Vehicle Details',
        'vehicle_details_subtitle' => 'some information about report',

        'utilization_summary_title' => 'Utilization Summary',
        'utilization_summary_subtitle' => 'some information about report',
    ],

    'contact_reports' => [
        'heading' => 'Contact reports',

        'contacts_list_title' => 'Contacts List',
        'contacts_list_subtitle' => 'some information about report',

        'contact_renewal_title' => 'Contact Renewal Reminders',
        'contact_renewal_subtitle' => 'some information about report',
    ],

    'fuel_reports' => [
        'heading' => 'Fuel Reports',

        'fuel_summary_title' => 'Fuel Summary',
        'fuel_summary_subtitle' => 'some information about report',

        'fuel_entries_by_vehicle_title' => 'Fuel Entries By Vehicle',
        'fuel_entries_by_vehicle_subtitle' => 'some information about report',
    ],

    'issues_reports' => [
        'heading' => 'Issues Reports',

        'issues_list_title' => 'Issues List',
        'issues_list_subtitle' => 'some information about report',
    ],

    'service_reports' => [
        'heading' => 'Service Reports',

        'service_summary_report_title' => 'Service Summary',
        'service_summary_report_subtitle' => 'some information about report',

        'service_history_report_title' => 'Service History By Vehicle',
        'service_history_report_subtitle' => 'some information about report',
    ],
];
