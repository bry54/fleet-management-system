<?php

return [
    'menu_title' => 'Issues Entries',
    'page_header_title' => 'Fleet Issues Management',
    'btn_home' => 'Issues Home',

    'btn_new_entry' => 'New Issue Entry',
    'lbl_new_fuel_entry' => 'Creating New Issues Entry',
    'msg_no_entries' => 'No issues entries found. Start by adding issues entries',
    'btn_create_entry' => 'CREATE A ISSUE ENTRY',
];
