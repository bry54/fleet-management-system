<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'username' => 'Username',
    'email_address' => 'Email Address',
    'password' => 'Password',
    'btn_sign_in' => 'Sign In',
    'btn_recover_password' => 'Recover Password',
    'btn_reset_password' => 'Forgot password?',
    'btn_want_login' => 'Want to login?',
    'lbl_reset_password' => 'Reset password',
    'lbl_sign_in' => 'Sign In',

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

];
