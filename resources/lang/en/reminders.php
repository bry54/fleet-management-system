<?php

return [
    'menu_title' => 'Reminders',
    'page_header_title' => 'Fleet Reminders Management',
    'btn_home' => 'Reminders Home',

    'btn_vehicles' => 'Vehicle Reminders',
    'btn_service' => 'Service Reminders',
    'btn_contacts' => 'Contact Reminders',

    'vehicles' => [
        'portlet_title' => 'Vehicle Reminders',
        'new_reminder_title' => 'Create Vehicle Reminder',
        'new_reminder' => 'New Vehicle Reminder',
        'msg_no_reminders' => 'There are no vehicle reminders, create reminders first',
    ],
    'services' => [
        'portlet_title' => 'Service Reminders',
        'new_reminder_title' => 'Create Service Reminder',
        'new_reminder' => 'New Service Reminder',
        'msg_no_reminders' => 'There are no service reminders, create reminders first',
    ],
    'contacts' => [
        'portlet_title' => 'Contact Reminders',
        'new_reminder_title' => 'Create Contact Reminder',
        'new_reminder' => 'New Contact Reminder',
        'msg_no_reminders' => 'There are no contact reminders, create reminders first',
    ],
];
