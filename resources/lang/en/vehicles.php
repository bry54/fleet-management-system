<?php

return [
    'menu_title' => 'Vehicles',
    'page_header_title' => 'Vehicles Management',
    'btn_home' => 'Vehicles Home',
    'btn_vehicle_assignment' => 'Vehicles Assignments',

    'tbl_name' => 'Vehicle Name',
    'tbl_status' => 'Status',
    'tbl_type' => 'Type',
    'tbl_last_recorded_meter' => 'Last Recorded Meter',
    'tbl_group' => 'Group',
    'tbl_assignment_status' => 'Assignment Availability',
    'tbl_assignee' => 'Assigned Operator',

];
