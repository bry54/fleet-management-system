<?php

return [
    'corporate_section' => 'Corporate Functions',

    'showcase' => [
        'title' => 'Fleet Management System',
        'subtitle' => 'The ultimate software solution for managing your fleet of vehicles'
    ],

    'user' => [
        'hie' => 'Hi, ',
        'profile_title' => 'My Profile',
        'profile_sub' => 'Account settings and more...',
        'message_title' => 'Messages',
        'message_sub' => 'View your secure inbox',
        'sign_out'=> 'Sign Out'
    ],

    'lang' => [
        'choose' => 'Select your prefered language',
        'english' => 'English',
        'turkish' => 'Turkish',
        'greek' => 'Greek',
    ],

    'actions' => [
        'title' => 'Quick User Actions',

        'vehicle_title' => 'New Vehicle',
        'vehicle_sub' => 'Create Vehicle',

        'fuel_title' => 'New Fuel Entry',
        'fuel_sub' => 'Create Fuel Entry',

        'issue_title' => 'New Issue',
        'issue_sub' => 'Create Issue',
    ],

    'footer' => [
        'team' => 'Team',
        'about' => 'About',
        'contact' => 'Contact',
        'copyright' => date("Y").' @ :company',
    ],

    'common_labels' => [
        'actions' => 'Actions',
        'export' => 'Export',
        'choose' => 'Choose Action',
        'print' => 'Print',
        'excel' => 'Excel',
        'save' => 'Save',
        'cancel' => 'Cancel',
    ]
];
