<?php

return [
    'menu_title' => 'Fuel Entries',
    'page_header_title' => 'Fuel Entries Management',
    'btn_home' => 'Fuel Home',
    'btn_new_entry' => 'New Fuel Entry',
    'lbl_new_fuel_entry' => 'Creating New Fuel Entry',
    'msg_no_entries' => 'No fuel entries found. Start by adding fuel entries',
    'btn_create_entry' => 'CREATE A FUEL ENTRY',

    'tbl_vehicle' => 'Vehicle',
    'tbl_date_time' => 'Date',
    'tbl_usage' => 'Usage',
    'tbl_volume' => 'Volume',
    'tbl_total' => 'Total',
    'tbl_fuel_economy' => 'Fuel Economny',
    'tbl_fuel_cost' => 'Cost',
    'tbl_entry_by' => 'Recorded By',
];
