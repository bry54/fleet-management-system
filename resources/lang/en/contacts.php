<?php

return [
    'menu_title' => 'Contacts',
    'page_header_title' => 'Contacts Management',
    'btn_home' => 'Contacts Home',
    'btn_export' => 'Export',
    'btn_all' => 'All Contacts',
    'btn_people' => 'People Contacts',
    'btn_companies' => 'Companies Contacts',
    'btn_new_company' => 'New Company',
    'btn_new_person' => 'New Person',

    'lbl_no_contacts' => 'No contacts entries found. Please start by adding contacts.',
    'btn_create_contacts' => 'Create Person | Company Contacts',
    'btn_create_company_contact' => 'Create Company Contact',
    'btn_create_people_contact' => 'Create Person Contact',

    'tbl_type' => 'Type',
    'tbl_name' => 'Name',
    'tbl_phone' => 'Phone',
    'tbl_email' => 'Email',
    'tbl_address' => 'Address',
    'tbl_actions' => 'Actions',

    'company_contacts' =>[
        'new_contact' => 'New Company Contact',
        'lbl_no_contacts' => 'No company contacts entries found. Please start by adding contacts.',
    ],

    'people_contacts' =>[
        'new_contact' => 'New Person Contact',
        'lbl_no_contacts' => 'No person contacts entries found. Please start by adding contacts.',
    ]
];
